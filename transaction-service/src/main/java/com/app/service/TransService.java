package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.app.model.Balance;
import com.app.model.Transaction;
import com.app.repository.TransactionServiceRepo;

public interface TransService {

	public List<Transaction> getAlltransactions();

	public Transaction saveTransDetails(Transaction trans);

	public Balance getUserBalance(String userName);


	



	// public Transaction getAlltransactionsById(String userId);

}
