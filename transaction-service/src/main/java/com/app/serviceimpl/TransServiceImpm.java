package com.app.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.BalanceDTO;
import com.app.model.Balance;
import com.app.model.Transaction;
import com.app.repository.BalanceRepo;
import com.app.repository.TransactionServiceRepo;
import com.app.service.TransService;

@Service
@Transactional
public class TransServiceImpm implements TransService {

	@Autowired
	TransactionServiceRepo transRepo;

	@Autowired
	BalanceRepo balanceRepo;

	@Override
	public List<Transaction> getAlltransactions() {

		System.out.println("Trans repo invoking");
		List<Transaction> transResult = transRepo.findAll();
		return transResult;
	}

	@Override
	public Transaction saveTransDetails(Transaction trans) {
		// TODO Auto-generated method stub

		Balance remainingBalance = balanceRepo.getByUserName(trans.getUserName().toString());
		if (trans.getTxType().equalsIgnoreCase("credit")) {
			Double result = null;
			if (null != remainingBalance)
				result = trans.getTxAmt() + remainingBalance.getBalance();

			System.out.println("result is :::---->" + result);
			// remainingBalance.setUserName(trans.getUserName());
			if (null != remainingBalance)
				remainingBalance.setBalance(result);
			balanceRepo.save(remainingBalance);
			transRepo.save(trans);
		} else {

			Double result = null;
			if (trans.getTxAmt() < remainingBalance.getBalance()) {
				result = remainingBalance.getBalance() - trans.getTxAmt();
				remainingBalance.setBalance(result);
				balanceRepo.save(remainingBalance);
				transRepo.save(trans);
			} else { // user cannot withdraw more than the available balance
				System.out.println("transaction aborted due to insufficient funds");
			}

		}

		return trans;
	}

	@Override
	public Balance getUserBalance(String userName) {

		Balance getBalance = balanceRepo.getByUserName(userName);
		return getBalance;
	}

}
