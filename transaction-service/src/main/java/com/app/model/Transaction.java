package com.app.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.app.dto.BalanceDTO;

import javax.persistence.Id;

@Entity
@Table(name = "TRANSACTION")
public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4818340345305153134L;
	private int transactionId;
	private String txType;
	private String fromAcc;
	private String toAcc;
	private Date ts;
	private int txAmt;
	private String userName;

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
		
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Transaction() {
	}

	@Column(name = "transaction_date")
	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name = "trans_type")
	public String getTxType() {
		return txType;
	}

	public void setTxType(String txType) {
		this.txType = txType;
	}

	@Column(name = "from_account")
	public String getFromAcc() {
		return fromAcc;
	}

	public void setFromAcc(String fromAcc) {
		this.fromAcc = fromAcc;
	}

	@Column(name = "to_account")
	public String getToAcc() {
		return toAcc;
	}

	public void setToAcc(String toAcc) {
		this.toAcc = toAcc;
	}

	@Column(name = "tx_amt")
	public int getTxAmt() {
		return txAmt;
	}

	public void setTxAmt(int txAmt) {
		this.txAmt = txAmt;
	}

	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", txType=" + txType + ", fromAcc=" + fromAcc
				+ ", toAcc=" + toAcc + ", txAmt=" + txAmt + ", userName=" + userName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromAcc == null) ? 0 : fromAcc.hashCode());
		result = prime * result + ((toAcc == null) ? 0 : toAcc.hashCode());
		result = prime * result + transactionId;
		result = prime * result + ((ts == null) ? 0 : ts.hashCode());
		result = prime * result + txAmt;
		result = prime * result + ((txType == null) ? 0 : txType.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (fromAcc == null) {
			if (other.fromAcc != null)
				return false;
		} else if (!fromAcc.equals(other.fromAcc))
			return false;
		if (toAcc == null) {
			if (other.toAcc != null)
				return false;
		} else if (!toAcc.equals(other.toAcc))
			return false;
		if (transactionId != other.transactionId)
			return false;
		if (ts == null) {
			if (other.ts != null)
				return false;
		} else if (!ts.equals(other.ts))
			return false;
		if (txAmt != other.txAmt)
			return false;
		if (txType == null) {
			if (other.txType != null)
				return false;
		} else if (!txType.equals(other.txType))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
