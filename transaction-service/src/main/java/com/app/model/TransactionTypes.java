package com.app.model;

public enum TransactionTypes {

	UPI,
	GENERAL,
	IMPS,
	NEFT;
	
}
