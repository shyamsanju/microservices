package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Balance;

@Repository
public interface BalanceRepo extends JpaRepository<Balance, Long> {
	
	Balance getByUserName(String username);
	

}
