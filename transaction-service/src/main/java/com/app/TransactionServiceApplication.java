package com.app;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
// import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.app.model.Balance;
import com.app.model.Transaction;
import com.app.repository.BalanceRepo;
import com.app.service.TransService;

@SpringBootApplication
@ComponentScan(basePackages={"com.app"})
@EnableResourceServer
@EnableTransactionManagement
@EnableDiscoveryClient
public class TransactionServiceApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(TransactionServiceApplication.class, args);
	}

	@Autowired
	public TransService service;
	
	@Autowired
	BalanceRepo balanceRepo;
	
	@Override
	public void run(String... args) throws Exception {
		
		Balance bal =new Balance();
		bal.setBalance(1000);
		bal.setId(0);
		bal.setUserName("ABNAMRO_CIB2");
		 balanceRepo.save(bal);
		
		Transaction trans =new Transaction();
		trans.setFromAcc("0");
		trans.setToAcc("1");
		trans.setTransactionId(0);
		trans.setTs(new Date());
		trans.setTxType("CREDIT");
		trans.setTxAmt(2000);
		trans.setUserName("ABNAMRO_CIB2");
		service.saveTransDetails(trans);
		
	}
}
