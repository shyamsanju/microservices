package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.Balance;
import com.app.model.Transaction;
import com.app.repository.TransactionServiceRepo;
import com.app.service.TransService;

@RestController
public class TransactionServiceController {

	@Autowired
	TransService transService;

	@Autowired
	TransactionServiceRepo transactionServiceRepo;

	@GetMapping(value = "/getAllTransactions")
	public List<Transaction> getAllTranscations() {

		System.out.println("Calling Service method");

		List<Transaction> transList = transService.getAlltransactions();

		// System.out.println("Transaction Data is :::::-------> " + transList);

		return transList;
	}

	/*
	 * @GetMapping(value = "/getAllTransactionsById", headers =
	 * "Accept=application/json") public Transaction
	 * getAllUserTranscations(@RequestParam("userId") String userId) {
	 * 
	 * System.out.println("Calling Service method--------------------------->"
	 * +userId);
	 * 
	 * Transaction transList = transService.getAlltransactionsById(userId);
	 * 
	 * System.out.println("Transaction Data is :::::-------> " + transList);
	 * 
	 * return transList; }
	 */

	@PostMapping(value = "/saveTransaction", headers = "Accept=application/json")
	public Transaction saveTransaction(@RequestBody Transaction trans) {
		// return transactionServiceRepo.save(trans);

		Transaction txObj = transService.saveTransDetails(trans);
		return txObj;

	}
	
	
	//public Balance  getBalance(@RequestBody Balance bal, @PathVariable("userName") String userName) {
	@GetMapping(value = "/getBalance/{userName}", headers = "Accept=application/json")
	public Balance  getBalance(@PathVariable("userName") String userName) {

		System.out.println("Calling Service method");

	Balance remainingBalance = transService.getUserBalance(userName);

		// System.out.println("Transaction Data is :::::-------> " + transList);

		return remainingBalance;
	}

}
