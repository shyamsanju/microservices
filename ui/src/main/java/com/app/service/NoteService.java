package com.app.service;

import java.util.Collection;
import java.util.List;

import com.app.dto.NoteDTO;
import com.app.model.MyJson;
import com.app.model.User;

public interface NoteService {
    Collection<NoteDTO> getAllNotes();
    NoteDTO addNote(NoteDTO note);
     <T> Object[] getAllUsers(); 
}
