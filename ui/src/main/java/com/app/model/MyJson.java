package com.app.model;

import java.util.List;

import org.springframework.stereotype.Component;
@Component
public class MyJson<T> {

	private List<T> List;
	
	public List<T> getList() {
		return List;
	}

	public void setList(List<T> List) {
		this.List = List;
	}
}
