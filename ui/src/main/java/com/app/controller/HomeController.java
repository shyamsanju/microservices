package com.app.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.dto.AccountDTO;
import com.app.dto.NoteDTO;
import com.app.model.Users;
import com.app.service.NoteService;


@Controller
public class HomeController {

    @Autowired
    NoteService noteService;

    @Autowired
    OAuth2RestTemplate restTemplate;
    
    @Value("${resource-server}/APIRest")
    private String restResourceContrl;
    
    @Value("${auth-server}/exit")
    private String logoutUrl;
    
    @Value("${auth-server}/rest")
    private String authServerCntrl;
    
	@Autowired
	private DiscoveryClient discoveryClient;
	
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    public ResponseEntity<AccountDTO> addUsers(@RequestBody AccountDTO user){
    	List<ServiceInstance> instances = discoveryClient.getInstances("ui-innovation-service");
		ServiceInstance serviceInstance = instances.get(0);

		String baseUrl = serviceInstance.getUri().toString();

		baseUrl = baseUrl + "/account-service/resource/v1/useraccounts";
    	System.out.println("test");
        ResponseEntity<AccountDTO> response = restTemplate.postForEntity(baseUrl, user, AccountDTO.class);
        return ResponseEntity.ok(response.getBody());
		
    	
    }
    

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:"+logoutUrl;//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    } 
    
    @RequestMapping("/")
    public String home() {
        return "redirect:"+"/dummy";
    }

    @RequestMapping("/dummy")
    public String notes(Model model) {
      //  model.addAttribute("notes", noteService.getAllNotes());
        return "index";
    }

    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("note", new NoteDTO());
        return "add";
    }
    @RequestMapping(value ="/getUsers" ,method=RequestMethod.GET)
    public Object[] add() {
    	
        return noteService.getAllUsers();
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(NoteDTO noteDTO, Model model) { 
        NoteDTO savedNote = noteService.addNote(noteDTO);

        if(savedNote != null){
            return "redirect:/notes";
        }else{
            model.addAttribute("note", noteDTO);
            return "add?error";
        }

    }
    
    @RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
    public ResponseEntity<Users> addUsers(@RequestBody Users user){
    	
        ResponseEntity<Users> response = restTemplate.postForEntity(restResourceContrl+"/addNew", user, Users.class);
	       
		return response;
    	
    }

    @RequestMapping("/sso/user")
    @SuppressWarnings("unchecked")
    public String user(Principal principal) {
        if (principal != null) {
            OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) principal;
            Authentication authentication = oAuth2Authentication.getUserAuthentication();
         return authentication.toString();
        }
		return null;
     
    }

}
