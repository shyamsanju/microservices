package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.app.model.AccountDTO;
import com.app.service.AccountService;

@SpringBootApplication
@ComponentScan(basePackages={"com.app"})
@EnableResourceServer
@EnableTransactionManagement
@EnableDiscoveryClient
public class AccountApplication implements CommandLineRunner{
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(AccountApplication.class, args);

	}
	
	@Autowired
	public AccountService service;

	@Override
	public void run(String... args) throws Exception {
		AccountDTO accountDTO = new AccountDTO();
		accountDTO.setId(0);
		accountDTO.setUsername("ABNAMRO_CIB2");
		service.createUser(accountDTO);
	}
	
}
