package com.app.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.app.model.AccountDTO;
@Component
public interface AccountService {

	public AccountDTO createUser(AccountDTO accountDTO);

	public List<AccountDTO> getAllUsers();

	public AccountDTO getUserById(int id);

}
