package com.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Account;
import com.app.model.AccountDTO;
import com.app.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	private Account accountDTOToAccount(AccountDTO accountDTO) {
		return new Account(accountDTO);
	}

	private AccountDTO accountToAccountDto(Account account) {
		return new AccountDTO(account);
	}	

	@Override
	public AccountDTO createUser(AccountDTO accountDTO) {
		Account account = accountRepository.save(accountDTOToAccount(accountDTO));
		return accountToAccountDto(account);
	}

	@Override
	public List<AccountDTO> getAllUsers() {
		List<Account> accounts;
		List<AccountDTO> accountDtos = new ArrayList<>();
		accounts = accountRepository.findAll();

		for (Account account : accounts) {
			accountDtos.add(accountToAccountDto(account));
		}
		return accountDtos;
	}

	@Override
	public AccountDTO getUserById(int id) {
		Account account = accountRepository.findOne(id);
		return accountToAccountDto(account);
	}

}
