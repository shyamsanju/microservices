package com.app.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.AccountDTO;
import com.app.service.AccountService;

@RestController
@RequestMapping("/v1/useraccounts")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping
	public ResponseEntity<AccountDTO> createUser(@RequestBody AccountDTO accountDTO) {
		try {
			accountDTO = accountService.createUser(accountDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<AccountDTO>(HttpStatus.CONFLICT);
		}
		return ResponseEntity.ok(accountDTO);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AccountDTO> getAllUsers() {
		List<AccountDTO> accountDTOS = null;
		try {
			accountDTOS = accountService.getAllUsers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accountDTOS;
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AccountDTO getUserById(@PathVariable("id") int id) {
		AccountDTO accountDTO = null;
		try {
			accountDTO = accountService.getUserById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accountDTO;
	}

	
	

}
