package com.app.model;

public class AccountDTO {

	public AccountDTO() {
	}
	
	public AccountDTO(Account account) {
		super();
		this.id = account.getId();
		this.username = account.getUsername();

	}

	private int id;
	private String username;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	

	
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", username=" + username + "]";
	}

}
