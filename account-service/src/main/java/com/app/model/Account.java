package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER_ACCOUNT"
		+ "")
public class Account {

	public Account() {
		super();
	}

	public Account(AccountDTO accountDTO) {
		super();
		this.id = accountDTO.getId();
		this.username = accountDTO.getUsername();
	
	}

	@Id
	@GeneratedValue
	@Column(name = "ID",unique=true)
	private int id;

	@Column(name = "USER_NAME",unique=true)
	private String username;


	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	

	@Override
	public String toString() {
		return "UserAccout [id=" + id + ", username=" + username +"]";
	}

}
